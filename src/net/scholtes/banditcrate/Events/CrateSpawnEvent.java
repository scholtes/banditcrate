package net.scholtes.banditcrate.Events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import net.scholtes.banditcrate.Main;

public class CrateSpawnEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	public CrateSpawnEvent() {
		
    }
	
	public static Main getMain() {
		return Main.getInstance();
	}

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
